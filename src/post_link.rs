use crate::date::UtcDate;
use crate::post::PostId;
use serde::Deserialize;
use serde_repr::Deserialize_repr;
use std::fmt::{Debug, Formatter};

/// Dump of all post links.
///
/// You can use this to parse a `PostsLinks.xml` file using, for example, `quick_xml`:
///
/// ```
/// use std::fs::File;
/// use std::io::BufReader;
/// use std::path::Path;
/// use quick_xml::de::from_reader;
/// use se_dump::post_link::{PostLinkId, PostLinks};
///
/// let reader = BufReader::new(File::open(Path::new("sample_data/PostLinks.xml")).unwrap());
/// let posts: PostLinks = from_reader(reader).unwrap();
/// assert_eq!(posts.posts[0].id, PostLinkId(104))
/// ```
#[derive(Deserialize, Debug)]
pub struct PostLinks {
    #[serde(rename = "$value", default)]
    pub posts: Vec<PostLink>,
}

#[derive(Deserialize, PartialOrd, PartialEq, Ord, Eq, Copy, Clone, Hash)]
pub struct PostLinkId(pub i32);

impl Debug for PostLinkId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

#[derive(Deserialize, Debug, PartialOrd, PartialEq, Ord, Eq, Clone, Hash)]
pub struct PostLink {
    /// Id primary key
    #[serde(rename = "@Id")]
    pub id: PostLinkId,

    /// CreationDate when the link was created
    #[serde(rename = "@CreationDate")]
    pub creation_date: UtcDate,

    /// PostId id of source post
    #[serde(rename = "@PostId")]
    pub post_id: PostId,

    /// RelatedPostId id of target/related post
    #[serde(rename = "@RelatedPostId")]
    pub related_post_id: PostId,

    /// LinkTypeId type of link
    #[serde(rename = "@LinkTypeId")]
    pub link_type: LinkType,
}

#[derive(Deserialize_repr, Debug, PartialOrd, PartialEq, Ord, Eq, Copy, Clone, Hash)]
#[repr(i32)]
pub enum LinkType {
    Linked = 1,
    Duplicate = 3,
}
