use crate::date::UtcDate;
use crate::user::UserId;
use serde::Deserialize;
use serde_repr::Deserialize_repr;
use std::fmt::{Debug, Formatter};

/// Dump of all posts.
///
/// You can use this to parse a `Posts.xml` file using, for example, `quick_xml`:
///
/// ```
/// use std::fs::File;
/// use std::io::BufReader;
/// use std::path::Path;
/// use quick_xml::de::from_reader;
/// use se_dump::post::{PostId, Posts};
///
/// let reader = BufReader::new(File::open(Path::new("sample_data/Posts.xml")).unwrap());
/// let posts: Posts = from_reader(reader).unwrap();
/// assert_eq!(posts.posts[0].id, PostId(2115))
/// ```
#[derive(Deserialize, Debug)]
pub struct Posts {
    #[serde(rename = "$value", default)]
    pub posts: Vec<Post>,
}

#[derive(Deserialize, PartialOrd, PartialEq, Ord, Eq, Copy, Clone, Hash)]
pub struct PostId(pub i32);

impl Debug for PostId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

// Would be kinda cool if this was an enum, with corresponding Question, Answer, etc types. Then we could avoid some Options. But https://github.com/serde-rs/serde/issues/745
#[derive(Deserialize, Debug, PartialOrd, PartialEq, Ord, Eq, Clone, Hash)]
pub struct Post {
    #[serde(rename = "@Id")]
    pub id: PostId,

    #[serde(rename = "@PostTypeId")]
    pub post_type: PostType,

    /// AcceptedAnswerId (only present on questions)
    #[serde(rename = "@AcceptedAnswerId")]
    pub accepted_answer_id: Option<PostId>,

    /// ParentId (only present on answers)
    #[serde(rename = "@ParentId")]
    pub parent_id: Option<PostId>,

    #[serde(rename = "@CreationDate")]
    pub creation_date: UtcDate,

    #[serde(rename = "@Score")]
    pub score: i32,

    #[serde(rename = "@ViewCount")]
    pub view_count: Option<i32>,

    /// Body (as rendered HTML, not Markdown)
    #[serde(rename = "@Body")]
    pub body: String,

    /// OwnerUserId (only present if user has not been deleted; always -1 for tag wiki entries, i.e. the community user owns them)
    #[serde(rename = "@OwnerUserId")]
    pub owner_user_id: Option<UserId>,

    #[serde(rename = "@OwnerDisplayName")]
    pub owner_display_name: Option<String>,

    #[serde(rename = "@LastEditorUserId")]
    pub last_editor_user_id: Option<UserId>,

    #[serde(rename = "@LastEditorDisplayName")]
    pub last_editor_display_name: Option<String>,

    #[serde(rename = "@LastEditDate")]
    pub last_edit_date: Option<UtcDate>,

    #[serde(rename = "@LastActivityDate")]
    pub last_activity_date: UtcDate,

    #[serde(rename = "@Title")]
    pub title: Option<String>,

    #[serde(rename = "@Tags")]
    pub tags: Option<String>,

    #[serde(rename = "@AnswerCount")]
    pub answer_count: Option<i32>,

    #[serde(rename = "@CommentCount")]
    pub comment_count: Option<i32>,

    #[serde(rename = "@FavoriteCount")]
    pub favorite_count: Option<i32>,

    #[serde(rename = "@ClosedDate")]
    pub closed_date: Option<UtcDate>,

    #[serde(rename = "@CommunityOwnedDate")]
    pub community_owned_date: Option<UtcDate>,

    #[serde(rename = "@ContentLicense")]
    pub content_license: String,
}

#[derive(Deserialize_repr, Debug, PartialOrd, PartialEq, Ord, Eq, Copy, Clone, Hash)]
#[repr(i32)]
pub enum PostType {
    Question = 1,
    Answer = 2,
    OrphanedTagWiki = 3,
    TagWikiExcerpt = 4,
    TagWiki = 5,
    ModeratorNomination = 6,
    WikiPlaceholder = 7,
    PrivilegeWiki = 8,
}
