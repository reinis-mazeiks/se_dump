use chrono::{DateTime, NaiveDateTime, Utc};
use serde::{Deserialize, Deserializer};
use std::fmt::{Debug, Formatter};

/// StackExchange dump dates are always specified in UTC, so we parse them accordingly
#[derive(PartialOrd, PartialEq, Ord, Eq, Copy, Clone, Hash)]
pub struct UtcDate(pub DateTime<Utc>);

impl<'de> Deserialize<'de> for UtcDate {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let naive_date = <NaiveDateTime as Deserialize>::deserialize(deserializer)?;
        let date = DateTime::from_utc(naive_date, Utc);
        Ok(UtcDate(date))
    }
}

impl Debug for UtcDate {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}
