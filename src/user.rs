use serde::Deserialize;
use std::fmt::{Debug, Formatter};

#[derive(Deserialize, PartialOrd, PartialEq, Ord, Eq, Copy, Clone, Hash)]
pub struct UserId(pub i32);

impl Debug for UserId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}
